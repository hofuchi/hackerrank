#! python3
import unittest


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class Solution:
    def __init__(self, T):
        self.T = int(T)

    def display(self, head):
        current = head
        while current:
            current = current.next

    def insert(self, head, data):
        try:
            self.listy.append(data)
        except Exception:
            self.listy = []
            self.listy.append(data)
        if len(self.listy) == self.T:
            global output
            output = (' '.join(map(str, self.listy)))

    def solve(self, data):
        self.data = data.split(' ')
        mylist = Solution(self.T)
        head = None
        for i in range(self.T):
            datay = self.data[i]
            head = mylist.insert(head, datay)
        mylist.display(head)
        return output


class MyTest(unittest.TestCase):
    def test_me(self):
        s = Solution(4)
        self.assertEqual(s.solve('2 3 4 1'), ('2 3 4 1'))  # OK


unittest.main()
