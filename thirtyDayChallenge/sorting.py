#! python3
import unittest
import sys


class Sorting:
    def __init__(self, l):
        self.lst = [int(l_temp) for l_temp in l.strip().split(' ')]
        self.n = len(self.lst

    def bubbleSort(self):
        swaps=0
        for number in range(self.n):
            for num in range(self.n):
                if num + 1 < self.n:
                    if self.lst[num] > self.lst[num + 1]:
                        self.lst[num], self.lst[num + 1]=self.lst[num + 1], self.lst[num]
                        swaps += 1

        return ("Array is sorted in %s swaps.\nFirst Element: %s\nLast Element: %s" % (swaps, self.lst[0], self.lst[self.n - 1]))


class MyTest(unittest.TestCase):
    def test_me(self):
        s=Sorting('9 8 7 6 5 4 3 2 1')
        self.assertEqual(
            s.bubbleSort(), ('Array is sorted in 36 swaps.\nFirst Element: 1\nLast Element: 9'))  # OK
        s=Sorting('1 5 2 4 3')
        self.assertEqual(s.bubbleSort(), ('Array is sorted in 4 swaps.\nFirst Element: 1\nLast Element: 5'))  # OK


if __name__ == '__main__':
    unittest.main()
