#! python3
import unittest


def helloWorld(input_string):
    return 'Hello, World.\n%s' % input_string


class MyTest(unittest.TestCase):
    def test_me(self):
        self.assertEqual(helloWorld('Graham'), "Hello, World.\nGraham")


unittest.main()
