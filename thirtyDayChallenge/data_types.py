#! python3
import unittest


def dataTypes(ii, dd, ss):
    i = 4
    d = 4.0
    s = 'HackerRank '

    # Print the sum of the data types.
    return i + ii, d + dd, s + ss


class MyTest(unittest.TestCase):
    def test_me(self):
        self.assertEqual(dataTypes(5, 5.0, 'is awesome'), (9, 9.0, 'HackerRank is awesome'))


unittest.main()
