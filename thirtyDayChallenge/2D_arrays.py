#! python3
import unittest
import sys


class TwoDArrays:
    def __init__(self):
        pass

    def twoDArrays(self, line):
        arr = []
        for arr_i in range(6):
            arr_t = [int(arr_temp) for arr_temp in (line[arr_i]).strip().split(' ')]
            arr.append(arr_t)
        zerox = 0
        onex = 1
        twox = 2
        zeroy = 0
        oney = 1
        twoy = 2
        rowsize = 6
        colsize = 6
        maxValueList = []
        for num in range(16):
            a = arr[zeroy][zerox]
            b = arr[zeroy][onex]
            c = arr[zeroy][twox]
            d = arr[oney][onex]
            e = arr[twoy][zerox]
            f = arr[twoy][onex]
            g = arr[twoy][twox]
            total = a + b + c + d + e + f + g
            maxValueList.append(total)
            zerox += 1
            onex += 1
            twox += 1
            if zerox == 4:
                zerox = 0
                onex = 1
                twox = 2
                zeroy += 1
                oney += 1
                twoy += 1
        return (max(maxValueList))


class MyTest(unittest.TestCase):
    def test_me(self):
        t = TwoDArrays()
        self.assertEqual(t.twoDArrays(['1 1 1 0 0 0', '0 1 0 0 0 0', '1 1 1 0 0 0',
                                       '0 0 2 4 4 0', '0 0 0 2 0 0', '0 0 1 2 4 0']), (19))  # OK


unittest.main()
