#! python3
import unittest
import sys


class BinaryNumbers:
    def __init__(self, number):
        self.number = int(number)
        self.binary = bin(self.number)
        self.binary = self.binary[2:]

    def consecutiveOnes(self):
        consecutiveOnes = 0
        historyList = [0]
        for num in range(len(self.binary)):
            if self.binary[num] == '0':
                historyList.append(consecutiveOnes)
                consecutiveOnes = 0
                continue
            elif self.binary[num] == '1':
                consecutiveOnes += 1
                historyList.append(consecutiveOnes)
                continue
            else:
                continue

        return (max(historyList))


class MyTest(unittest.TestCase):
    def test_me(self):
        b = BinaryNumbers(13)
        self.assertEqual(b.consecutiveOnes(), (2))  # OK
        b = BinaryNumbers(15)
        self.assertEqual(b.consecutiveOnes(), (4))  # OK


unittest.main()
