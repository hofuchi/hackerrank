#! python3
import unittest
import sys


class Solution:
    def __init__(self):
        pass

    def bitwiseAND(self, t, inputy):
        inputy = inputy.split('\n')
        returnstring = ''
        for a0 in range(t):
            outputlist = []
            numberlist = []
            n, k = inputy[a0].strip().split(' ')
            n, k = [int(n), int(k)]
            for num in range(n):
                numberlist.append(num)
            for num in range(n):
                for number in range(n):
                    if n - number > num + 1:
                        outputlist.append(int(numberlist[num + 1]) & int(n - number))
            returnstring += str(max(v for v in outputlist if v < k)) + '\n'

        return returnstring


class MyTest(unittest.TestCase):
    def test_me(self):
        s = Solution()
        self.assertEqual(s.bitwiseAND(3, '5 2\n8 5\n2 2'), ('1\n4\n0\n'))  # OK


unittest.main()
