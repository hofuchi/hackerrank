#! python3
import unittest


class Difference:
    def __init__(self, a, _):
        self._ = int(_)
        self.__elements = a
        self.maximumDifference = []
        self.a = [int(e) for e in a.split(' ')]

    def computeDifference(self):
        # a outputs [1,2,5]
        for num in range(len(self.a)):
            for number in range(int(self._)):
                self.maximumDifference.append(abs(self.a[number] - self.a[num]))
        self.maximumDifference = max(self.maximumDifference)

    def solve(self):
        d.computeDifference()
        return(d.maximumDifference)


class MyTest(unittest.TestCase):
    def test_me(self):
        global d
        d = Difference('1 2 5', 3)
        self.assertEqual(d.solve(), (4))  # OK


unittest.main()
