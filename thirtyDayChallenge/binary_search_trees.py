#! python3
import unittest


class Node:
    def __init__(self, data):
        self.right = self.left = None
        self.data = data


class Solution:
    def insert(self, root, data):
        if root is None:
            return Node(data)
        else:
            if data <= root.data:
                cur = self.insert(root.left, data)
                root.left = cur
            else:
                cur = self.insert(root.right, data)
                root.right = cur
        return root

    def getHeight(self, root):
        if root is None:
            return -1
        else:
            return max(Solution().getHeight(root.left), Solution().getHeight(root.right)) + 1

    def input(self, inpt):
        inpt = inpt.split(' ')
        root = None
        for i in range(int(inpt[0])):
            data = int(inpt[i + 1])
            root = Solution().insert(root, data)

        return Solution().getHeight(root)


class MyTest(unittest.TestCase):
    def test_me(self):
        s = Solution()
        self.assertEqual(s.input('7 3 5 2 1 4 6 7'), (3))


unittest.main()
