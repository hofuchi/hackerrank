#! python3
import unittest


class Factorial:
    def __init__(self):
        pass

    def factorial(self, number):
        self.number = int(number)
        if self.number == 1:
            return 1
        else:
            return self.number * Factorial().factorial(self.number - 1)


class MyTest(unittest.TestCase):
    def test_me(self):
        f = Factorial()
        self.assertEqual(f.factorial(6), (720))  # OK


unittest.main()
