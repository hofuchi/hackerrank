#! python3
import unittest


class Person:
    def __init__(self, firstName, lastName, idNumber):
        self.firstName = firstName
        self.lastName = lastName
        self.idNumber = idNumber

    def printPerson(self):
        global output
        output = ''
        output += 'Name: %s, %s\n' % (self.lastName, self.firstName)
        output += 'ID: %s\n' % (self.idNumber)


class Student(Person):
    def __init__(self, firstName, lastName, idNumber, scores):
        self.firstName = firstName
        self.lastName = lastName
        self.idNumber = idNumber
        self.scores = scores

    def calculate(self):
        totalScores = 0
        for num in range(len(self.scores)):
            totalScores += self.scores[num - 1]
        averageScores = totalScores / len(self.scores)
        if averageScores >= 90 and averageScores <= 100:
            return 'O'
        if averageScores >= 80 and averageScores < 90:
            return 'E'
        if averageScores >= 70 and averageScores < 80:
            return 'A'
        if averageScores >= 55 and averageScores < 70:
            return 'P'
        if averageScores >= 40 and averageScores < 55:
            return 'D'
        if averageScores >= 0 and averageScores < 40:
            return 'T'


class Input:
    def __init__(self, i):
        line = i.split()
        self.firstName = line[0]
        self.lastName = line[1]
        self.idNum = line[2]
        self.numScores = line[3]
        self.scores = list(map(int, line[4].split(',')))

    def solve(self):
        s = Student(self.firstName, self.lastName, self.idNum, self.scores)
        s.printPerson()
        global output
        output += 'Grade: %s' % (s.calculate())
        return output


class MyTest(unittest.TestCase):
    def test_me(self):
        i = Input('Heraldo Memelli 8135627 2 100,80')
        self.assertEqual(i.solve(), ('Name: Memelli, Heraldo\nID: 8135627\nGrade: O'))  # OK


unittest.main()
