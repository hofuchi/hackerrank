#! python3
import unittest
import sys


class Array:
    def __init__(self, n, arr):
        self.n = int(n)
        self.arr = [int(arr_temp) for arr_temp in arr.strip().split(' ')]

    def reverse_the_array(self):
        for num in range(int(self.n / 2)):
            self.arr[num], self.arr[self.n - num - 1] = self.arr[self.n - num - 1], self.arr[num]
        return (' '.join(str(e) for e in self.arr))


class MyTest(unittest.TestCase):
    def test_me(self):
        a = Array(4, '1 4 3 2')
        self.assertEqual(a.reverse_the_array(), ('2 3 4 1'))  # OK
        a = Array(5, '1 4 3 2 2')
        self.assertEqual(a.reverse_the_array(), ('2 2 3 4 1'))  # OK


unittest.main()
