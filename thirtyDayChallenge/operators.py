#! python3
import unittest


class Operators:
    def __init__(self, meal_cost, tip_percent, tax_percent):
        self.meal_cost = meal_cost
        self.tip_percent = float(tip_percent)
        self.tax_percent = float(tax_percent)

    def get_total_cost_of_meal(self):
        # Calculation code here
        tip = self.meal_cost * (self.tip_percent / 100)  # calculate tip
        tax = self.meal_cost * (self.tax_percent / 100)  # caclulate tax
        # cast the result of the rounding operation to an int and save it as total_cost
        total_cost = int(round(self.meal_cost + tip + tax))

        return str(total_cost)


class MyTest(unittest.TestCase):
    def test_me(self):
        o = Operators(12.00, 20, 8)
        self.assertEqual(o.get_total_cost_of_meal(), ('15'))  # OK


unittest.main()
