import fileinput

'''
This problem is a programming version of Problem 185 from projecteuler.net

The game Number Mind is a variant of the well known game Master Mind.

Instead of coloured pegs, you have to guess a secret sequence of digits. After each guess you're only
told in how many places you've guessed the correct digit. So, if the sequence was  and you guessed ,
you'd be told that you have one correct digit; however, you would NOT be told that you also have
another digit in the wrong place.

Based on the some guesses, find the unique 12-digit secret sequence.

Input Format

First line of every input file contains a single integer -- the number of guesses. The lines following
each contain the 12-digit guess sequence; and the number of correct digits for this guess .

Output Format

Output the string with exactly  digits  the unique valid answer to the guesses.

Sample Input

20
228569150065 1
907564288621 0
496954400043 0
713459943615 0
211421327491 1
258317293172 0
919252724339 1
197103476352 0
151173430038 0
063794395936 0
504759866532 1
502906565456 0
790539816536 0
595873942664 1
346602334981 0
988808475766 1
559203789779 0
498580144863 1
441454897857 1
622818801178 0

Sample Output

884045122207
'''


def main():
    # Works for the sample test case provided
    # Doesn't work for most inputs,
    # ... Including rows with more than one correct number
    correctGueses = []
    incorrectGuesses = []
    # allGuesses = []
    with fileinput.input(files=('euler185Input.txt')) as f:
        for line in f:
            if fileinput.isfirstline() is True:
                numGuesses = int(line)
                continue
            splitLine = line.split(' ')
            numCorrect = int(splitLine[1])
            guess = (splitLine[0])
            if numCorrect < 1:
                incorrectGuesses.append(list(guess))
            else:
                correctGueses.append(list(guess))
            # allGuesses.append(list(guess))
            #
            # Outputs:
            # 228569150065 1
            # 211421327491 1
            # 919252724339 1
            # 504759866532 1
            # 595873942664 1
            # 988808475766 1
            # 498580144863 1
            # 441454897857 1
            # 907564288621 0
            # 496954400043 0
            # 713459943615 0
            # 258317293172 0
            # 197103476352 0
            # 151173430038 0
            # 063794395936 0
            # 502906565456 0
            # 790539816536 0
            # 346602334981 0
            # 559203789779 0
            # 622818801178 0
            # Correct answer is:
            # 884045122207
            #
            # As Arrays
            # [2,2,8,5,6,9,1,5,0,0,6,5] 1
            # [2,1,1,4,2,1,3,2,7,4,9,1] 1
            # [9,1,9,2,5,2,7,2,4,3,3,9] 1
            # [5,0,4,7,5,9,8,6,6,5,3,2] 1
            # [5,9,5,8,7,3,9,4,2,6,6,4] 1
            # [9,8,8,8,0,8,4,7,5,7,6,6] 1
            # [4,9,8,5,8,0,1,4,4,8,6,3] 1
            # [4,4,1,4,5,4,8,9,7,8,5,7] 1
            # [9,0,7,5,6,4,2,8,8,6,2,1] 0
            # [4,9,6,9,5,4,4,0,0,0,4,3] 0
            # [7,1,3,4,5,9,9,4,3,6,1,5] 0
            # [2,5,8,3,1,7,2,9,3,1,7,2] 0
            # [1,9,7,1,0,3,4,7,6,3,5,2] 0
            # [1,5,1,1,7,3,4,3,0,0,3,8] 0
            # [0,6,3,7,9,4,3,9,5,9,3,6] 0
            # [5,0,2,9,0,6,5,6,5,4,5,6] 0
            # [7,9,0,5,3,9,8,1,6,5,3,6] 0
            # [3,4,6,6,0,2,3,3,4,9,8,1] 0
            # [5,5,9,2,0,3,7,8,9,7,7,9] 0
            # [6,2,2,8,1,8,8,0,1,1,7,8] 0
            # Correct answer is:
            # [8,8,4,0,4,5,1,2,2,2,0,7]
            #
            # _ substituted for incorrect
            # [_,_,_,_,_,_,1,_,_,_,_,_] 1
            # [_,_,_,_,_,_,_,2,_,_,_,_] 1
            # [_,_,_,_,_,_,_,2,_,_,_,_] 1
            # [_,_,4,_,_,_,_,_,_,_,_,_] 1
            # [_,_,_,_,_,_,_,_,2,_,_,_] 1
            # [_,8,_,_,_,_,_,_,_,_,_,_] 1
            # [_,_,_,_,_,_,1,_,_,_,_,_] 1
            # [_,_,_,_,_,_,_,_,_,_,_,7] 1
            # Correct answer is:
            # [8,8,4,0,4,5,1,2,2,2,0,7]
            #
            # _ substituted for correct
            # [2,2,8,5,6,9,_,5,0,0,6,5] 1
            # [2,1,1,4,2,1,3,_,7,4,9,1] 1
            # [9,1,9,2,5,2,7,_,4,3,3,9] 1
            # [5,0,_,7,5,9,8,6,6,5,3,2] 1
            # [5,9,5,8,7,3,9,4,2,6,6,4] 1
            # [9,_,8,8,0,8,4,7,5,7,6,6] 1
            # [4,9,8,5,8,0,_,4,4,8,6,3] 1
            # [4,4,1,4,5,4,8,9,7,8,5,_] 1
            # Correct answer is:
            # [8,8,4,0,4,5,1,2,2,2,0,7]
            #
            # _ For columns with not no answer
            # [2,_,_,5,6,9,_,_,_,0,6,_] 1
            # [2,_,_,4,2,1,_,_,_,4,9,_] 1
            # [9,_,_,2,5,2,_,_,_,3,3,_] 1
            # [5,_,_,7,5,9,_,_,_,5,3,_] 1
            # [5,_,_,8,7,3,_,_,_,6,6,_] 1
            # [4,_,_,5,8,0,_,_,_,8,6,_] 1
            # [9,_,_,8,0,8,_,_,_,7,6,_] 1
            # [4,_,_,4,5,4,_,_,_,8,5,_] 1
            # [9,_,_,5,6,4,_,_,_,6,2,_] 0
            # [4,_,_,9,5,4,_,_,_,0,4,_] 0
            # [7,_,_,4,5,9,_,_,_,6,1,_] 0
            # [2,_,_,3,1,7,_,_,_,1,7,_] 0
            # [1,_,_,1,0,3,_,_,_,3,5,_] 0
            # [1,_,_,1,7,3,_,_,_,0,3,_] 0
            # [0,_,_,7,9,4,_,_,_,9,3,_] 0
            # [5,_,_,9,0,6,_,_,_,4,5,_] 0
            # [7,_,_,5,3,9,_,_,_,5,3,_] 0
            # [3,_,_,6,0,2,_,_,_,9,8,_] 0
            # [5,_,_,2,0,3,_,_,_,7,7,_] 0
            # [6,_,_,8,1,8,_,_,_,1,7,_] 0
            # Correct answer is:
            # [8,8,4,0,4,5,1,2,2,2,0,7]

    # orderedByColumn = []
    correctOrderedByColumn = []
    incorrectOrderedByColumn = []
    # for col in range(12):
    #    temp = []
    #    for row in range(numGuesses):
    #        temp.append(int(allGuesses[row][col]))
    #    orderedByColumn.append(temp)
    for col in range(12):
        temp = []
        for row in range(len(correctGueses)):
            temp.append(int(correctGueses[row][col]))
        correctOrderedByColumn.append(temp)
    for col in range(12):
        temp = []
        for row in range(len(incorrectGuesses)):
            temp.append(int(incorrectGuesses[row][col]))
        incorrectOrderedByColumn.append(temp)

    numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    answerList = []
    taken = []
    for col in range(12):
        if (set(correctOrderedByColumn[col]) < set(incorrectOrderedByColumn[col])):
            # 8, 0
            answerList.append(list(set(numbers) - set(correctOrderedByColumn[col] + incorrectOrderedByColumn[col]))[0])
        else:
            if len(set(correctOrderedByColumn[col]) - set(incorrectOrderedByColumn[col])) > 1:
                if len(set(numbers) - set(correctOrderedByColumn[col] + incorrectOrderedByColumn[col])) > 0:
                    # 4, 5, 0, (7 from the if) -- everything else from the else:
                    correctNum = list(set(correctOrderedByColumn[col]) - set(incorrectOrderedByColumn[col]))
                    correctList = [x for x in correctNum if x not in taken]
                    if len(correctList) == 1:
                        answerList.append(correctList[0])
                    else:
                        answerList.append(list(set(numbers) - set(correctOrderedByColumn[col] + incorrectOrderedByColumn[col]))[0])
                else:
                    # 4, 2, 2
                    correctNum = list(set(correctOrderedByColumn[col]) - set(incorrectOrderedByColumn[col]))[0]
                    # Not yet accounting for duplicate answers in the same column
                    taken.append(correctOrderedByColumn[col].index(correctNum))
                    answerList.append(correctNum)
            else:
                if len(list(set(numbers) - set(correctOrderedByColumn[col] + incorrectOrderedByColumn[col]))) > 1:
                    # 8, 1
                    correctNum = list(set(correctOrderedByColumn[col]) - set(incorrectOrderedByColumn[col]))[0]
                    # Not yet accounting for duplicate answers in the same column
                    taken.append(correctOrderedByColumn[col].index(correctNum))
                    answerList.append(correctNum)
                else:
                    # 2
                    answerList.append(list(set(numbers) - set(correctOrderedByColumn[col] + incorrectOrderedByColumn[col]))[0])

    print(int(''.join(map(str, answerList))))


if __name__ == '__main__':
    main()
